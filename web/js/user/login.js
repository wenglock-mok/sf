/*
 * This file is part of the UserBundle.
 * The login component.
 */

// in case a browser does not have a console.
if (!console) {
    window.console = {
        log: function() {}
    };
}

/**
 * Start angular module
 */
(function() {
    var app = angular.module('sfApp');

    /**
     * Service to POST subscription to the backend API
     */
    app.factory(
        'LoginService',
        ['BaseService', function(BaseService) {
            var service = Object.create(BaseService);

            /**
             * Calls the login service to authenticate a user
             * @param user
             * @return {Promise}
             */
            service.login = function(user) {
                var url = Routing.generate('user.do_ajax_login');
                var params = { _username: user.email, _password: user.password };

                return service.doPost(url, params);
            };

            return service;
        }]
    );

    app.controller(
        'LoginController',
        ['$scope', '$filter', '$timeout', '$window', 'LoginService',
            function($scope, $filter, $timeout, $window, LoginService) {
                var $ctrl = this;

                /**
                 * Logs a user in by running the authentication service
                 */
                $ctrl.login = function() {
                    $ctrl.submitting = true;
                    $ctrl.error = null;
                    $ctrl.errorStatus = null;

                    LoginService.login($ctrl.user)
                        .then(function(data) {
                            $ctrl.success = true;

                            $window.location.href = Routing.generate('homepage');
                        }).catch(function(response) {
                            if (response.status && response.status == 403) {
                                $ctrl.error = 'Your login details do not appear to be correct. Please try again.'

                                if (response.data.status) {
                                    if (response.data.status == 'NVEF') {
                                        $ctrl.error = 'Your account does not appear to have been verified. We would have sent you an email when you registered with a link to verify your account. Please verify your account and try logging in again.';
                                    } else if (response.data.status == 'ACDB') {
                                        $ctrl.error = 'Your account seems to have been disabled. Please contact us for further information.';
                                    }
                                }
                            } else {
                                $ctrl.error = 'It looks like we encountered a problem logging you in to SecretFlag. Please try again later, or contact us if you need further help.';
                            }
                        }).finally(function() {
                            $ctrl.submitting = false;
                        });
                };
            }
        ]
    );
}());