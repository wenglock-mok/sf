/*
 * This file is part of the UserBundle.
 * The password reset component.
 */

// in case a browser does not have a console.
if (!console) {
    window.console = {
        log: function() {}
    };
}

/**
 * Start angular module
 */
(function() {
    var app = angular.module('sfApp');

    /**
     * Service to POST subscription to the backend API
     */
    app.factory(
        'PasswordResetService',
        ['BaseService', function(BaseService) {
            var service = Object.create(BaseService);

            /**
             * Calls the reset service to send an email to reset a user's password
             * @param user
             * @param url
             * @return {Promise}
             */
            service.request = function(user, url) {console.log(url);
                if (url) {
                    var params = { username: user.email };

                    return service.doPost(url, params);
                } else {
                    throw 'Missing URL for requesting a password reset';
                }
            };

            return service;
        }]
    );

    app.controller(
        'ResetController',
        ['$scope', '$filter', '$timeout', '$window', '$attrs', 'PasswordResetService',
            function($scope, $filter, $timeout, $window, $attrs, PasswordResetService) {
                var $ctrl = this;

                /**
                 * Submits the password reset request
                 */
                $ctrl.request = function() {
                    $ctrl.submitting = true;
                    $ctrl.error = null;

                    PasswordResetService.request($ctrl.user, $attrs['passwordResetUrl'])
                        .then(function(data) {
                            $ctrl.success = true;

                            // $window.location.href = Routing.generate('homepage');
                        }).catch(function(response) {
                            if (response.status && response.status == 403) {
                                $ctrl.error = 'Your login details do not appear to be correct. Please try again.'
                            } else {
                                $ctrl.error = 'It looks like we encountered a problem logging you in to SecretFlag. Please try again later, or contact us if you need further help.';
                            }
                        }).finally(function() {
                            $ctrl.submitting = false;
                        });
                };
            }
        ]
    );

    app.controller(
        'PasswordResetController',
        ['$scope',
            function($scope) {
                var $ctrl = this;

                /**
                 * Runs init functionality when dom is ready
                 */
                angular.element(document).ready(function() {
                    // remap the form elements into more readable format
                    $ctrl.form.password1 = $ctrl.form['fos_user_resetting_form[plainPassword][first]'];
                    $ctrl.form.password2 = $ctrl.form['fos_user_resetting_form[plainPassword][second]'];
                });

                /**
                 * Validates the confirmation password matches
                 */
                $scope.$watchGroup(['$ctrl.user.password', '$ctrl.user.password_confirm'], function(values) {
                    if (values[0] && values[1]) {
                        $ctrl.form.password2.$setValidity('mismatch', values[0] == values[1]);
                    }
                });

                /**
                 * Submit the given form
                 */
                $ctrl.submitForm = function() {
                    $ctrl.submitting = true;
                };
            }
        ]
    );
}());