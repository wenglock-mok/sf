/*
 * This file is part of the UserBundle.
 * The registration component which collects information required to complete a simple registration.
 */

// in case a browser does not have a console.
if (!console) {
    window.console = {
        log: function() {}
    };
}

/**
 * Start angular module
 */
(function() {
    var app = angular.module('sfApp');

    /**
     * Service to POST subscription to the backend API
     */
    app.factory(
        'UserRegistrationService',
        ['BaseService', function(BaseService) {
            var service = Object.create(BaseService);

            /**
             * Calls the registration service to register a new user
             * @param user
             * @return {Promise}
             */
            service.register = function(user) {
                var url = Routing.generate('user.api.do_register');
                var params = user;

                return service.doPost(url, params);
            };

            return service;
        }]
    );

    app.controller(
        'UserRegistrationController',
        ['$scope', '$filter', '$timeout', '$window', '$http', 'UserRegistrationService',
            function($scope, $filter, $timeout, $window, $http, UserRegistrationService) {

            }
        ]
    );

    userRegistrationController.$inject = ['$scope', '$filter', '$timeout', '$window', '$http', 'UserRegistrationService'];
    function userRegistrationController($scope, $filter, $timeout, $window, $http, UserRegistrationService) {
        var $ctrl = this;

        /**
         * Validates the confirmation password matches
         */
        $scope.$watchGroup(['$ctrl.user.password', '$ctrl.user.password_confirm'], function(values) {
            if (values[0] && values[1]) {
                $ctrl.form.password2.$setValidity('mismatch', values[0] == values[1]);
            }
        });

        /**
         * Validates the birthdate
         */
        $scope.$watchGroup(['$ctrl.user.birth_day', '$ctrl.user.birth_month', '$ctrl.user.birth_year'], function(values) {
            if (values[0] && values[1] && values[2]) {
                // test if this works with moment.
                var bd = moment(values[2] + '-' + values[1] + '-' + values[0], 'YYYY-M-D');
                $ctrl.form.birthDay.$setValidity('invalid_day', bd.isValid());
            }
        });

        /**
         *
         */
        $ctrl.$onInit = function() {

        };

        /**
         * Function that posts the subscription email to the backend
         */
        $ctrl.submitRegistration = function() { console.log('submitting');
            $ctrl.submitting = true;
            $ctrl.isDupe = false;
            $ctrl.error = null;

            UserRegistrationService.register($ctrl.user)
                .then(function(data) {
                    $ctrl.success = true;
                }).catch(function(response) {console.log(response);
                    if (response.status && response.status == 409) {
                        $ctrl.form.email.$setValidity('dupe', false);
                    } else {
                        $ctrl.error = 'It looks like we encountered a problem registering you for SecretFlag. Please try again later, or contact us if you need further help.';
                    }
                }).finally(function() {
                    $ctrl.submitting = false;
                });
            // $http({
            //     method: 'POST',
            //     url: Routing.generate('preview.api.subscribe'),
            //     data: { email: $ctrl.email },
            // }).then(
            //     function success(response) {
            //         $ctrl.submitting = false;
            //         $ctrl.showThankYou = true;
            //     },
            //     function error(response) {
            //         $ctrl.submitting = false;
            //
            //         if (response.status != 200) {
            //             if (response.status == 400) {
            //                 $ctrl.emailInvalid = true;
            //             } else if (response.status == 403) {
            //                 $ctrl.isDupe = true;
            //             } else {
            //                 $ctrl.otherError = true;
            //             }
            //         }
            //     }
            // );
        };
    }

    /**
     * Component definition
     */
    app.component('userRegistration', {
        templateUrl: 'user_registration_form_template.html',
        bindings: { },
        controller: userRegistrationController
    });
}());