<?php

namespace UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Class UserContact
 * @package UserBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="user_contacts")
 */
class UserContact
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="UserBundle\Entity\User", fetch="EXTRA_LAZY", inversedBy="contacts")
     * @ORM\JoinColumn(name="sf_user", referencedColumnName="id")
     *
     * @var User
     */
    protected $user;

    /**
     * @ORM\Column(name="name", type="text")
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="relationship", type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $relationship;

    /**
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(name="phone", type="string", length=100, nullable=true)
     *
     * @var string
     */
    protected $phone;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return UserContact
     */
    public function setUser($user)
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return UserContact
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getRelationship()
    {
        return $this->relationship;
    }

    /**
     * @param string $relationship
     * @return UserContact
     */
    public function setRelationship($relationship)
    {
        $this->relationship = $relationship;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return UserContact
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     * @return UserContact
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }
}