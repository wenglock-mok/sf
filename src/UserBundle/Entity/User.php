<?php

namespace UserBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;

use AppBundle\Entity\Currency;

/**
 * Class User
 * @package UserBundle\Entity
 *
 * @ORM\Entity()
 * @ORM\Table(name="users")
 */
class User extends BaseUser
{
    const STATUS_UNDERAGED = 'UA';
    const STATUS_PENDING_VERIFICATION = 'NV';
    const STATUS_VERIFIED = 'UV';
    const STATUS_ID_VALID = 'OK';
    const STATUS_SUSPENDED = 'SP';

    const UNDERAGE_BOUNDARY_YEARS = 18;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="full_name", type="text", nullable=true)
     *
     * The full name of this user
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="given_name", type="text", nullable=true)
     *
     * The given name of this user
     * @var string
     */
    protected $givenName;

    /**
     * @ORM\Column(name="family_name", type="text", nullable=true)
     *
     * The family name of this user
     * @var string
     */
    protected $familyName;

    /**
     * @ORM\Column(name="display_name", type="string", length=50, nullable=true)
     *
     * The preferred display name for this user
     * @var string
     */
    protected $displayName;

    /**
     * @ORM\Column(name="birthdate", type="datetime", nullable=true)
     *
     * The user's birthdate
     * @var \DateTime
     */
    protected $birthdate;

    /**
     * @ORM\Column(name="phone_number", type="string", length=100, nullable=true)
     *
     * This user's phone number (including the country code)
     * @var string
     */
    protected $phoneNumber;

    /**
     * @ORM\Column(name="full_address", type="string", length=255, nullable=true)
     *
     * The full address of this user's residence
     * todo may need to split this in future
     * @var string
     */
    protected $fullAddress;

    /**
     * @ORM\Column(name="address_place_id", type="string", length=50, nullable=true)
     *
     * The Google Place ID of the user's address
     * @var string
     */
    protected $addressPlaceId;

    /**
     * @ORM\Column(name="address_url", type="string", length=255, nullable=true)
     *
     * The Google Place URL of this user's address
     * @var string
     */
    protected $addressUrl;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Language", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(
     *     name="user_languages",
     *     joinColumns={@ORM\JoinColumn(name="user", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="language", referencedColumnName="id")}
     * )
     *
     * A collection of languages spoken by this user
     * @var Collection
     */
    protected $languages;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Currency", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="currency", referencedColumnName="id", nullable=true)
     *
     * The currency selected by the user
     * @var Currency
     */
    protected $currency;

    /**
     * @ORM\Column(name="description", type="text", nullable=true)
     *
     * A user-specified description of him/herself
     * @var string
     */
    protected $description;

    /**
     * @ORM\OneToMany(targetEntity="UserBundle\Entity\UserContact", fetch="EXTRA_LAZY", mappedBy="user")
     *
     * A collection of this user's contacts
     * @var Collection
     */
    protected $contacts;

    /**
     * @ORM\Column(name="registration_time", type="datetime")
     *
     * The date/time this user registered
     * @var \DateTime
     */
    protected $registrationTime;

    /**
     * @ORM\Column(name="last_reminder_time", type="datetime", nullable=true)
     *
     * When this user was last reminded to verify his/her account
     * @var \DateTime
     */
    protected $lastReminderTime;

    /**
     * @ORM\Column(name="verification_time", type="datetime", nullable=true)
     *
     * When this user was verified
     * @var \DateTime
     */
    protected $verificationTime;

    /**
     * @ORM\Column(name="last_update_time", type="datetime")
     *
     * When this user's profile was last updated
     * @var \DateTime
     */
    protected $lastUpdateTime;

    /**
     * @ORM\Column(name="status", type="string", length=2, nullable=true)
     *
     * The current status of this user
     * @var string
     */
    protected $status;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->givenName = null;
        $this->familyName = null;
        $this->displayName = null;
        $this->birthdate = null;
        $this->languages = new ArrayCollection();
        $this->contacts = new ArrayCollection();
        $this->status = null;
        $this->registrationTime = new \DateTime();
        $this->lastUpdateTime = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }

    /**
     * @param \DateTime $birthdate
     * @return User
     */
    public function setBirthdate(\DateTime $birthdate)
    {
        $this->birthdate = $birthdate;
        return $this;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @param string $phoneNumber
     * @return User
     */
    public function setPhoneNumber($phoneNumber)
    {
        $this->phoneNumber = $phoneNumber;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressPlaceId()
    {
        return $this->addressPlaceId;
    }

    /**
     * @param string $addressPlaceId
     * @return User
     */
    public function setAddressPlaceId($addressPlaceId)
    {
        $this->addressPlaceId = $addressPlaceId;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddressUrl()
    {
        return $this->addressUrl;
    }

    /**
     * @param string $addressUrl
     * @return User
     */
    public function setAddressUrl($addressUrl)
    {
        $this->addressUrl = $addressUrl;
        return $this;
    }

    /**
     * @return string
     */
    public function getFullAddress()
    {
        return $this->fullAddress;
    }

    /**
     * @param string $fullAddress
     * @return User
     */
    public function setFullAddress($fullAddress)
    {
        $this->fullAddress = $fullAddress;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param Collection $languages
     * @return User
     */
    public function setLanguages(Collection $languages)
    {
        $this->languages = $languages;
        return $this;
    }

    /**
     * @return Currency
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param Currency $currency
     * @return User
     */
    public function setCurrency(Currency $currency)
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return User
     */
    public function setDescription($description)
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getContacts()
    {
        return $this->contacts;
    }

    /**
     * @param Collection $contacts
     * @return User
     */
    public function setContacts(Collection $contacts)
    {
        $this->contacts = $contacts;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getRegistrationTime()
    {
        return $this->registrationTime;
    }

    /**
     * @param \DateTime $registrationTime
     * @return User
     */
    public function setRegistrationTime(\DateTime $registrationTime)
    {
        $this->registrationTime = $registrationTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastReminderTime()
    {
        return $this->lastReminderTime;
    }

    /**
     * @param \DateTime $lastReminderTime
     * @return User
     */
    public function setLastReminderTime(\DateTime $lastReminderTime = null)
    {
        $this->lastReminderTime = $lastReminderTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getVerificationTime()
    {
        return $this->verificationTime;
    }

    /**
     * @param \DateTime $verificationTime
     * @return User
     */
    public function setVerificationTime(\DateTime $verificationTime = null)
    {
        $this->verificationTime = $verificationTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getLastUpdateTime()
    {
        return $this->lastUpdateTime;
    }

    /**
     * @param \DateTime $lastUpdateTime
     * @return User
     */
    public function setLastUpdateTime(\DateTime $lastUpdateTime)
    {
        $this->lastUpdateTime = $lastUpdateTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * @param string $displayName
     * @return User
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;
        return $this;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getGivenName()
    {
        return $this->givenName;
    }

    /**
     * @param string $givenName
     * @return User
     */
    public function setGivenName($givenName)
    {
        $this->givenName = $givenName;
        return $this;
    }

    /**
     * @return string
     */
    public function getFamilyName()
    {
        return $this->familyName;
    }

    /**
     * @param string $familyName
     * @return User
     */
    public function setFamilyName($familyName)
    {
        $this->familyName = $familyName;
        return $this;
    }

    /**
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param string $status
     * @return User
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }

    /**
     * Indicates if this user is under 18 as of today based on his/her birthday
     * @return bool
     */
    public function isUnderaged()
    {
        if (empty($this->birthdate)) {
            return false;
        }

        try {
            /** @var \DateInterval $diff */
            $diff = (new \DateTime())->diff($this->birthdate);

            // underaged if the year difference is less than the boundary
            return $diff->y < self::UNDERAGE_BOUNDARY_YEARS;
        } catch (\Exception $ex) {
            return false;
        }
    }

    /**
     * Returns the abbreviated single character name for the bubble
     * @return string
     */
    public function getAbbreviatedName()
    {
        if (!empty($this->name)) {
            // split by space
            $tokens = explode(' ', $this->name);
            if (count($tokens) > 1) {
                return strtoupper(sprintf('%s%s', substr($tokens[0], 0, 1), substr($tokens[count($tokens)-1], 0, 1)));
            } else {
                return strtoupper(substr($tokens[0], 0, 1));
            }
        } else {
            return 'U';
        }
    }
}