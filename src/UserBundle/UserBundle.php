<?php

namespace UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle that contains user-related functionality
 *
 * @package UserBundle
 */
class UserBundle extends Bundle
{
    /**
     * {@inheritDoc}
     * @see \Symfony\Component\HttpKernel\Bundle\Bundle::getParent()
     */
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
