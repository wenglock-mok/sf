<?php

namespace UserBundle\Controller;

use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use UserBundle\Entity\User;

/**
 * Class RegistrationController
 * @package UserBundle\Controller
 */
class RegistrationController extends Controller
{
    /**
     * @Route(path="/verify/{token}", name="user.do_verify", methods={"GET"})
     *
     * Controller action that verifies a user's registration based on a given confirmation token
     * @param string $token
     * @return Response
     * @throws \Exception
     */
    public function doVerifyAction($token)
    {
        // first ensure that the confirmation token is found
        // confirm the user does not already exist
        $om = $this->getDoctrine()->getManager();
        try {
            $user = $om->getRepository('UserBundle:User')->findOneBy(array('confirmationToken' => $token));
            if (empty($user)) {
                throw new HttpException(Response::HTTP_NOT_FOUND, 'Page not found');
            }

            // if the user is already active, then send him straight to the home page
            if (!empty($user->isEnabled())) {
                return $this->redirectToRoute('homepage', array());
            }

            // otherwise, enable the user
            $user->setEnabled(true);

            $now = new \DateTime();
            $user->setLastUpdateTime($now);
            $user->setVerificationTime($now);

            // underaged users cannot be verified
            if (empty($user->isUnderaged())) {
                $user->setStatus(User::STATUS_VERIFIED);
            }

            $om->persist($user);
            $om->flush();

            return $this->render('user/verification_result.html.twig', array('message' => 'Welcome onboard - thank you for verifying your registration!'));
        } catch (\Exception $ex) {
            if ($ex instanceof HttpException) {
                throw $ex;
            } else {
                throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $ex->getMessage());
            }
        }
    }

    /**
     * @Route(path="/api/user/do-register", name="user.api.do_register", methods={"POST"}, options={"expose": true})
     *
     * API controller action that processes a registration form submission
     * Required inputs:
     * - full_name
     * - email
     * - password
     * - password_confirm
     * - birth_day
     * - birth_month
     * - birth_year
     *
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @param LoggerInterface $logger
     * @return JsonResponse
     * @throws \Exception
     */
    public function doRegisterAction(Request $request, UserPasswordEncoderInterface $encoder, LoggerInterface $logger)
    {
        $REQUEST_PARAMS = array('full_name', 'email', 'password', 'password_confirm', 'birth_day', 'birth_month', 'birth_year');
        foreach ($REQUEST_PARAMS as $param) {
            if (empty($request->request->has($param))) {
                throw new HttpException(Response::HTTP_BAD_REQUEST, sprintf('Missing parameter: %s', $param));
            }
        }

        // confirm the two passwords match
        if ($request->request->get('password') != $request->request->get('password_confirm')) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, sprintf('Mismatched passwords'));
        }

        // confirm the user does not already exist
        $om = $this->getDoctrine()->getManager();
        try {
            $dupe = $om->getRepository('UserBundle:User')->findOneBy(array('emailCanonical' => strtolower($request->request->get('email'))));
        } catch (\Exception $ex) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $ex->getMessage());
        }

        if (!empty($dupe)) {
            throw new HttpException(Response::HTTP_CONFLICT, sprintf('User with email %s already exists', $request->request->get('email')));
        }

        // check the birthdate
        try {
            $birthdate = new \DateTime(sprintf('%04d-%02d-%02d', $request->request->get('birth_year'), $request->request->get('birth_month'), $request->request->get('birth_day')));
        } catch (\Exception $ex) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, sprintf('Invalid birthdate format: %s', sprintf('%04d-%02d-%02d', $request->request->get('birth_year'), $request->request->get('birth_month'), $request->request->get('birth_day'))));
        }

        $now = new \DateTime();

        // create the user
        $newUser = new User();
        $newUser->setName($request->request->get('full_name'));
        $newUser->setDisplayName($newUser->getName());

        $newUser->setEmail($request->request->get('email'));
        $newUser->setEmailCanonical(strtolower($request->request->get('email')));

        $newUser->setBirthdate($birthdate);
        if ($newUser->isUnderaged()) {
            $newUser->setStatus(User::STATUS_UNDERAGED);
        } else {
            $newUser->setStatus(User::STATUS_PENDING_VERIFICATION);
        }

        // new users not enabled by default
        $newUser->setEnabled(false);
        $newUser->setSuperAdmin(false);
        $newUser->setUsername($newUser->getEmail());
        $newUser->setUsernameCanonical($newUser->getEmailCanonical());
        // MUST set the salt first before encoding the password
        $newUser->setSalt(bin2hex(random_bytes(32)));
        $newUser->setPassword($encoder->encodePassword($newUser, $request->request->get('password')));
        $newUser->setConfirmationToken($this->get('fos_user.util.token_generator')->generateToken());
        $newUser->setRoles(array('ROLE_NOT_VALIDATED', 'ROLE_USER'));
        $newUser->setRegistrationTime($now);
        $newUser->setLastUpdateTime($now);

        try {
            $om->persist($newUser);
            $om->flush();

            // send verification email
            $message = new \Swift_Message('Thank you for registering to join SecretFlag');
            $message->setFrom('admin@secretflag.com', 'SecretFlag');
            $message->setTo($newUser->getEmail());
            $message->setBody(
                $this->renderView('email/registration/validation.html.twig', array('user' => $newUser)),
                'text/html'
            );

            $this->get('mailer')->send($message);
        } catch (\Exception $ex) {
            if ($ex instanceof HttpException) {
                throw $ex;
            } else {
                throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $ex->getMessage());
            }
        }

        return new JsonResponse(array('status' => $newUser->getStatus()), Response::HTTP_OK);
    }

    /**
     * @Route(path="/register", name="user.register", options={"expose": true})
     *
     * Controller action that displays the registration form
     * @return Response
     */
    public function showRegisterAction()
    {
        return $this->render('user/register.html.twig', array());
    }

    /**
     * @param int $id
     * @return Response
     */
    public function sampleRegisterEmailAction($id)
    {
        $om = $this->getDoctrine()->getManager();
        try {
            $user = $om->getRepository('UserBundle:User')->findOneById($id);
        } catch (\Exception $ex) {
            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $ex->getMessage());
        }

        if (!empty($user)) {
            return $this->render('email/registration/validation.html.twig', array('user' => $user));
        }
    }
}