<?php

namespace UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use UserBundle\Entity\User;

/**
 * Class LoginController
 * @package UserBundle\Controller
 */
class LoginController extends Controller
{
    const STATUS_NOT_VERIFIED = 'NVEF';
    const STATUS_ACCOUNT_DISABLED = 'ACDB';
    const STATUS_INVALID_CRED = 'INVC';

    /**
     * @Route(path="/api/login", name="user.do_ajax_login", methods={"POST"}, options={"expose": true})
     *
     * Controller action that processes an AJAX login
     * @param Request $request
     * @param UserPasswordEncoderInterface $encoder
     * @return JsonResponse
     */
    public function doAjaxLoginAction(Request $request, UserPasswordEncoderInterface $encoder)
    {
        // find user in the database
        $user = $this->getDoctrine()->getRepository('UserBundle:User')->findOneBy(array('username' => $request->request->get('_username')));
        if (!empty($user)) {
            // check password against what was entered
            if ($encoder->isPasswordValid($user, $request->request->get('_password'))) {
                if ($user->isEnabled()) {
                    $this->get('fos_user.security.login_manager')->logInUser('main', $user);
                } else {
                    // not enabled - possibly not validated
                    if ($user->getStatus() == User::STATUS_PENDING_VERIFICATION) {
                        $status = self::STATUS_NOT_VERIFIED;
                    } else {
                        $status = self::STATUS_ACCOUNT_DISABLED;
                    }
                }
            }
        }

        // basically, if this login was successful, the token should have been created and stored
        if (!$this->get('security.authorization_checker')->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            if (empty($status)) {
                $status = self::STATUS_INVALID_CRED;
            }

            return new JsonResponse(array('status' => $status), Response::HTTP_FORBIDDEN);
        } else {
            return new JsonResponse(array(), Response::HTTP_OK);
        }
    }
}