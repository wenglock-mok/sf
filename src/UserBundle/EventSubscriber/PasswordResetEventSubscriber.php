<?php

namespace UserBundle\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\RouterInterface;
use FOS\UserBundle\FOSUserEvents;
use FOS\UserBundle\Event\GetResponseUserEvent;
use FOS\UserBundle\Event\FormEvent;
use Psr\Log\LoggerInterface;

/**
 * Listens for password reset events
 *
 * Class PasswordResetEventSubscriber
 * @package UserBundle\EventSubscriber
 */
class PasswordResetEventSubscriber implements EventSubscriberInterface
{
    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * PasswordResetEventSubscriber constructor.
     * @param RouterInterface $router
     * @param LoggerInterface $logger
     */
    public function __construct(RouterInterface $router, LoggerInterface $logger)
    {
        $this->router = $router;
        $this->logger = $logger;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents()
    {
        return array(
            FOSUserEvents::RESETTING_SEND_EMAIL_COMPLETED => array(
                array('returnJsonResponse', 10)
            ),
            FOSUserEvents::RESETTING_RESET_SUCCESS => array(
                array('redirectToHomeResponse', 10)
            )
        );
    }

    /**
     * Changes the response to xhr if the request was through ajax
     * @param GetResponseUserEvent $event
     */
    public function returnJsonResponse(GetResponseUserEvent $event)
    {
        if ($event->getRequest()->isXmlHttpRequest()) {
            $event->setResponse(new JsonResponse(array('status' => 'ok'), Response::HTTP_OK));
        }
    }

    /**
     * Changes the response to redirect to home upon password reset success
     * @param FormEvent $event
     */
    public function redirectToHomeResponse(FormEvent $event)
    {
        $event->setResponse(new RedirectResponse($this->router->generate('homepage')));
    }
}