<?php

namespace PreviewBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="preview_subscribers", indexes={
 *     @ORM\Index(name="preview_subscribers_email_idx", columns={"email"})
 * })
 *
 * Represents a subscriber to the preview of SF
 * @package PreviewBundle\Entity
 */
class PreviewSubscriber
{
    /**
     * @ORM\Id()
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * Identity field
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="email", type="string", length=255, unique=true)
     *
     * Email address of this subscriber
     * @var string
     */
    protected $email;

    /**
     * @ORM\Column(name="remote_host", type="string", length=255)
     *
     * The IP address of this subscriber
     * @var string
     */
    protected $remoteHost;

    /**
     * @ORM\Column(name="client_platform", type="text", nullable=true)
     *
     * The client platform of this subscriber
     * @var string
     */
    protected $clientPlatform;

    /**
     * @ORM\Column(name="subscription_time", type="datetime")
     *
     * When this subscription was created
     * @var \DateTime
     */
    protected $subscriptionTime;

    /**
     * @ORM\Column(name="validation_token", type="string", length=32)
     *
     * A 32-character random string generated for each subscription that is used to validate unsubscribe
     * (and other stuff in future)
     * @var string
     */
    protected $validationToken;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return PreviewSubscriber
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getRemoteHost()
    {
        return $this->remoteHost;
    }

    /**
     * @param string $remoteHost
     * @return PreviewSubscriber
     */
    public function setRemoteHost($remoteHost)
    {
        $this->remoteHost = $remoteHost;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientPlatform()
    {
        return $this->clientPlatform;
    }

    /**
     * @param string $clientPlatform
     * @return PreviewSubscriber
     */
    public function setClientPlatform($clientPlatform = null)
    {
        $this->clientPlatform = $clientPlatform;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getSubscriptionTime()
    {
        return $this->subscriptionTime;
    }

    /**
     * @param \DateTime $subscriptionTime
     * @return PreviewSubscriber
     */
    public function setSubscriptionTime(\DateTime $subscriptionTime)
    {
        $this->subscriptionTime = $subscriptionTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getValidationToken()
    {
        return $this->validationToken;
    }

    /**
     * @param string $validationToken
     * @return PreviewSubscriber
     */
    public function setValidationToken($validationToken)
    {
        $this->validationToken = $validationToken;
        return $this;
    }
}