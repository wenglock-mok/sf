<?php

namespace PreviewBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Bundle that contains preview-related functionality
 * E.g. a subscription service
 *
 * @package PreviewBundle
 */
class PreviewBundle extends Bundle
{
}
