<?php

namespace PreviewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Container for all preview-related pages
 * @package PreviewBundle\Controller
 */
class DefaultController extends Controller
{
    /**
     * Returns the homepage of the SF preview
     * @param Request $request
     * @return Response
     */
    public function previewAction(Request $request)
    {
        return $this->render('@Preview/preview/index.html.twig');
    }
}