<?php

namespace PreviewBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

use PreviewBundle\Entity\PreviewSubscriber;

/**
 * Contains controller actions for preview subscription-related functionality
 * @package PreviewBundle\Controller
 */
class SubscriptionController extends Controller
{
    /**
     * API action that records a subscription to the preview
     * @param Request $request
     * @return JsonResponse
     * @throws \Exception
     */
    public function subscribeAction(Request $request)
    {
        $remoteHost = $request->getClientIp();
        $clientPlatform = $request->headers->get('User-Agent');
        $timestamp = new \DateTime();

        // check the request to make sure email is set
        if (!$request->request->has('email')) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'An email address was not supplied for subscription');
        }

        $email = $request->request->get('email');
        try {
            $em = $this->getDoctrine()->getManager();

            // look for dupes.
            $duplicate = $em->getRepository('PreviewBundle:PreviewSubscriber')->findOneByEmail($email);
            if (!empty($duplicate)) {
                throw new HttpException(Response::HTTP_FORBIDDEN, sprintf('%s has already been subscribed', $email));
            }

            // validate the email address
            if (empty(filter_var($email, FILTER_VALIDATE_EMAIL))) {
                throw new HttpException(Response::HTTP_BAD_REQUEST, 'Invalid email address supplied');
            }

            // create a new subscription
            $subscription = new PreviewSubscriber();
            $subscription->setEmail($email);
            $subscription->setRemoteHost($remoteHost);
            $subscription->setClientPlatform($clientPlatform);
            $subscription->setSubscriptionTime($timestamp);

            // generate a random token for this subscription
            $subscription->setValidationToken(substr(base64_encode(openssl_random_pseudo_bytes(30)), 0, 32));

            // commit to the database
            $em->persist($subscription);
            $em->flush();

            // send email
            $message = new \Swift_Message('Thank you for subscribing to SecretFlag');
            $message->setFrom('admin@secretflag.com', 'SecretFlag');
            $message->setTo($subscription->getEmail());
            $message->setBody(
                $this->renderView('@Preview/email/thankyou.html.twig', array('subscription' => $subscription)),
                'text/html'
            );

            $this->get('mailer')->send($message);
        } catch (\Exception $ex) {
            if ($ex instanceof HttpException) {
                throw $ex;
            }

            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $ex->getMessage());
        }

        return new JsonResponse(array(), Response::HTTP_OK);
    }

    /**
     * API action that unsubscribes an email from the preview
     * @param string $id
     * @param string $token
     * @return Response
     * @throws \Exception
     */
    public function unsubscribeAction($id, $token)
    {
        try {
            $em = $this->getDoctrine()->getManager();

            // try to get this subscription if it exists.
            /** @var PreviewSubscriber $subscription */
            $subscription = $em->getRepository('PreviewBundle:PreviewSubscriber')->findOneById($id);
            if (empty($subscription)) {
                throw new HttpException(Response::HTTP_BAD_REQUEST, 'Error unsubscribing this user');
            }

            // validate the security token
            if (urldecode($token) != $subscription->getValidationToken()) {
                throw new HttpException(Response::HTTP_BAD_REQUEST, 'Error unsubscribing this user');
            }

            // if everything matches, remove from the list.
            $em->remove($subscription);
            $em->flush();
        } catch (\Exception $ex) {
            if ($ex instanceof HttpException) {
                throw $ex;
            }

            throw new HttpException(Response::HTTP_INTERNAL_SERVER_ERROR, $ex->getMessage());
        }

        return $this->render('@Preview/preview/unsub.html.twig', array());
    }
}