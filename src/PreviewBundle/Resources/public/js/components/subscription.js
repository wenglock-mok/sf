/*
 * This file is part of the PreviewBundle.
 * The subscription component which collects an email address to subscribe to site updates.
 */

// in case a browser does not have a console.
if (!console) {
    window.console = {
        log: function() {}
    };
}

/**
 * Start angular module
 */
(function() {
    var app = angular.module('sfApp');

    /**
     * Service to POST subscription to the backend API
     */
    app.factory(
        'PreviewSubscriptionService',
        ['BaseService', function(BaseService) {
            var service = Object.create(BaseService);

            /**
             * Creates a subscription with the given email address
             * @param emailAddress
             * @return {Promise}
             */
            service.subscribe = function(emailAddress) {
                var url = Routing.generate('preview.api.subscribe');
                var params = { email: emailAddress };

                return service.doPost(url, params);
            };

            return service;
        }]
    );

    previewSubscriptionController.$inject = ['$scope', '$filter', '$timeout', '$window', '$http', 'PreviewSubscriptionService'];
    function previewSubscriptionController($scope, $filter, $timeout, $window, $http, PreviewSubscriptionService) {
        var $ctrl = this;

        /**
         * Defaults to mailing list
         */
        $ctrl.$onInit = function() {
            $ctrl.toggleSlide('mailing-list');
        };

        /**
         * Function that toggles the display of the different slides
         * @param s
         */
        $ctrl.toggleSlide = function(s) {
            $ctrl.slide = s;
        };

        /**
         * Function that posts the subscription email to the backend
         */
        $ctrl.submitSubscription = function() {
            $ctrl.submitting = true;
            $ctrl.emailInvalid = false;
            $ctrl.isDupe = false;
            $ctrl.otherError = false;

            // PreviewSubscriptionService.subscribe($ctrl.email)
            //     .then(
            //         function(s) {
            //             $ctrl.submitting = false;
            //             $ctrl.showThankYou = true;
            //         },
            //         function(f, c) {
            //             $ctrl.submitting = false;
            //
            //             // figure out why there was an error
            //             console.log(c);
            //             console.log(f);
            //             if (c == 400 && f.match(/Invalid email address/)) {
            //                 $ctrl.emailInvalid = true;
            //             }
            //         }
            //     );
            // run the http call to the remote service to retrieve the rule translation.
            $http({
                method: 'POST',
                url: Routing.generate('preview.api.subscribe'),
                data: { email: $ctrl.email },
            }).then(
                function success(response) {
                    $ctrl.submitting = false;
                    $ctrl.showThankYou = true;
                },
                function error(response) {
                    $ctrl.submitting = false;

                    if (response.status != 200) {
                        if (response.status == 400) {
                            $ctrl.emailInvalid = true;
                        } else if (response.status == 403) {
                            $ctrl.isDupe = true;
                        } else {
                            $ctrl.otherError = true;
                        }
                    }
                }
            );
        };
    }

    /**
     * Component definition
     */
    app.component('previewSubscription', {
        templateUrl: 'preview_subscription_template.html',
        bindings: { },
        controller: previewSubscriptionController
    });
}());