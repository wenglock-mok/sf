<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Psr\Log\LoggerInterface;

use AppBundle\Entity\Country;
use AppBundle\Entity\City;
use AppBundle\Entity\Currency;
use AppBundle\Entity\Language;

/**
 * A sync service for countries & related data from restcountries.eu
 *
 * Class CountriesSyncService
 * @package AppBundle\Service
 */
class CountriesSyncService
{
    const ENDPOINT = 'https://restcountries.eu/rest/v2';
    const ALL_CODES_API = '/all?fields=alpha2Code';
    const ONE_COUNTRY_API = '/alpha/%s';

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * CountriesSyncService constructor.
     * @param EntityManagerInterface $em
     * @param LoggerInterface $logger
     */
    public function __construct(EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->em = $em;
        $this->logger = $logger;
    }

    /**
     * Syncs all countries
     */
    public function syncAll()
    {
        // grab the JSON string from the endpoint.
        $endpoint = self::ENDPOINT . self::ALL_CODES_API;
        $this->logger->info(sprintf('Grabbing JSON from %s', $endpoint));
        $response = file_get_contents($endpoint);
        if (!empty($response)) {
            $allCountriesJson = json_decode($response, true);
            $this->logger->info(sprintf('Syncing %d countries', count($allCountriesJson)));

            foreach ($allCountriesJson as $oneCountryJson) {
                $this->syncOneCountry($oneCountryJson['alpha2Code']);
            }
        }
        $this->logger->info('Completed all countries sync');
    }

    /**
     * Syncs one country using the alpha2code（2-character code)
     * @param string $alpha2code
     */
    public function syncOneCountry($alpha2code)
    {
        // grab the JSON string from the endpoint.
        $endpoint = sprintf(self::ENDPOINT . self::ONE_COUNTRY_API, $alpha2code);
        $this->logger->info(sprintf('Grabbing JSON from %s', $endpoint));
        $response = file_get_contents($endpoint);
        if (!empty($response)) {
            $countryJson = json_decode($response, true);
            if (!empty($countryJson)) {
                // start syncing
                // find country using the alpha 2 code first.
                $country = $this->em->getRepository('AppBundle:Country')->findOneBy(array('alpha2Code' => $countryJson['alpha2Code']));
                if (empty($country)) {
                    $country = new Country();
                    $country->setName($countryJson['name']);
                    $country->setEffDate((new \DateTime())->setTime(0, 0, 0, 0));

                    $this->em->persist($country);
                } else {
                    if ($country->getName() != $countryJson['name']) {
                        // todo if name has changed, do we want to create a new entry?
                    }
                }

                if (!empty($countryJson['topLevelDomain'])) {
                    $country->setTopLevelDomain($countryJson['topLevelDomain'][0]);
                }
                $country->setAlpha2Code($countryJson['alpha2Code']);
                $country->setAlpha3Code($countryJson['alpha3Code']);
                if (empty($countryJson['region'])) {
                    $country->setRegion(null);
                } else {
                    $country->setRegion($countryJson['region']);
                }
                if (empty($countryJson['subregion'])) {
                    $country->setSubRegion(null);
                } else {
                    $country->setSubRegion($countryJson['subregion']);
                }
                $country->setFlagUrl($countryJson['flag']);

                // find the capital city if it exists.
                if (!empty($countryJson['capital'])) {
                    if (empty($country->getCapital())) {
                        $capital = new City();
                        $capital->setCountry($country);
                        $capital->setName($countryJson['capital']);
                        $country->setCapital($capital);

                        $this->em->persist($capital);
                    }
                }

                // sync all currencies
                foreach ($countryJson['currencies'] as $currencyJson) {
                    // find currency
                    $currency = $this->em->getRepository('AppBundle:Currency')->findOneBy(array('code' => $currencyJson['code']));
                    if (empty($currency)) {
                        $currency = new Currency();
                        if (!empty($currencyJson['code']) && $currencyJson['code'] != '(none)') {
                            $currency->setCode($currencyJson['code']);
                        } else {
                            $currency->setCode(null);
                        }

                        if (empty($currencyJson['name'])) {
                            $currency->setName(sprintf('%s currency', $country->getName()));
                        } else {
                            $currency->setName($currencyJson['name']);
                        }

                        $currency->setSymbol($currencyJson['symbol']);
                        $currency->setEffDate((new \DateTime())->setTime(0, 0, 0, 0));

                        $this->em->persist($currency);
                    }

                    // check if country in currency.
                    if (!$currency->getCountries()->contains($country)) {
                        $currency->getCountries()->add($country);
                    }
                }

                // sync all languages
                foreach ($countryJson['languages'] as $languageJson) {
                    // find language
                    $language = $this->em->getRepository('AppBundle:Language')->findOneBy(array('alpha3Code' => $languageJson['iso639_2']));
                    if (empty($language)) {
                        $language = new Language();
                        $language->setAlpha2Code($languageJson['iso639_1']);
                        $language->setAlpha3Code($languageJson['iso639_2']);
                        $language->setName($languageJson['name']);
                        $language->setNativeName($languageJson['nativeName']);

                        $this->em->persist($language);
                    }

                    // check if country in language.
                    if (!$language->getCountries()->contains($country)) {
                        $language->getCountries()->add($country);
                    }
                }

                $this->em->flush();
                $this->em->clear();

                $this->logger->info(sprintf('Completed sync for %s (%s)', $countryJson['name'], $countryJson['alpha3Code']));
            }
        }
    }
}