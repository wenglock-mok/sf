<?php
/*
 * This file is part of the AppBundle.
 */

namespace AppBundle\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * A listener to listen for exceptions thrown for XHR requests and to return a corresponding JSON response
 */
class XhrExceptionListener
{
    /**
     * Fires whenever a kernel exception is thrown and convert HttpException into json response so angular can process the error messages
     * With thanks to http://stackoverflow.com/questions/10583428/is-there-a-symfony2-event-handler-for-session-timeout-a-k-a-not-logged-in.
     * @param GetResponseForExceptionEvent $event The event that has been caught by this listener.
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        // get the incoming HTTP request.
        $request = $event->getRequest();

        // ignore if not XHR.
        if (!$request->isXmlHttpRequest()) {
            return;
        }

        $exception = $event->getException();

        if (!$exception instanceof HttpException) {
            return;
        }

        $response = new JsonResponse(array('error' => $exception->getMessage()), $exception->getStatusCode());

        $event->setResponse($response);
        $event->stopPropagation();
    }
}