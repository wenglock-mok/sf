<?php
/*
 * This file is part of the AppBundle.
 */

namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * A listener to add an XSRF token to every response
 */
class CsrfTokenCookieListener
{
    const XSRF_DEFAULT_ID = 'SF';
    const XSRF_COOKIE = 'XSRF-TOKEN';

    /**
     * The symfony xsrf token manager
     * @var CsrfTokenManagerInterface
     */
    private $tokenManager;

    /**
     * CsrfTokenCookieListener constructor.
     * @param CsrfTokenManagerInterface $tokenManager
     */
    public function __construct(CsrfTokenManagerInterface $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    /**
     * Adds a XSRF token cookie to each response
     * @param FilterResponseEvent $event
     */
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $request = $event->getRequest();
        $response = $event->getResponse();

        // create the token
        $tokenId = empty($request->getUser()) ? self::XSRF_DEFAULT_ID : $request->getUser();
        $token = $this->tokenManager->getToken($tokenId);

        // create the cookie and expire it an hour later
        $cookieExpiry = (new \DateTime())->add(new \DateInterval('PT60M'));
        $cookie = new Cookie(self::XSRF_COOKIE, $token->getValue(), $cookieExpiry, '/', null, false, false);
        $response->headers->setCookie($cookie);
    }
}