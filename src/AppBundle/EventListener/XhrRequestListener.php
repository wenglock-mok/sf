<?php
/*
 * This file is part of the AppBundle.
 */

namespace AppBundle\EventListener;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Csrf\CsrfToken;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;

/**
 * A listener to listen for xhr requests
 */
class XhrRequestListener
{
    const XSRF_DEFAULT_ID = 'SF';
    const XSRF_HEADER = 'X-XSRF-TOKEN';

    /**
     * The symfony xsrf token manager
     * @var CsrfTokenManagerInterface
     */
    private $tokenManager;

    /**
     * XhrRequestListener constructor.
     * @param CsrfTokenManagerInterface $tokenManager
     */
    public function __construct(CsrfTokenManagerInterface $tokenManager)
    {
        $this->tokenManager = $tokenManager;
    }

    /**
     * Listens for xhr POST requests and verifies the given XSRF token
     * Assumes the XSRF token has been sent in the X-XSRF-TOKEN header
     * @param GetResponseEvent $event The event that has been caught by this listener
     * @throws HttpException If the xhr POST request is not valid
     */
    public function verifyXsrfToken(GetResponseEvent $event)
    {
        // get the incoming HTTP request.
        $request = $event->getRequest();

        // ignore if not XHR or not POST.
        if (!$request->isXmlHttpRequest() || $request->getMethod() != Request::METHOD_POST) {
            return;
        }

        // grab the XSRF token
        $xsrfToken = $request->headers->get(self::XSRF_HEADER);
        if (empty($xsrfToken)) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Missing XSRF token');
        }

        // verify the XSRF token
        // check the ID: if anonymous, use SF, otherwise use the user ID
        $id = empty($request->getUser()) ? self::XSRF_DEFAULT_ID : $request->getUser();
        $token = new CsrfToken($id, $xsrfToken);
        if (!$this->tokenManager->isTokenValid($token)) {
            throw new HttpException(Response::HTTP_BAD_REQUEST, 'Invalid XSRF token');
        }
    }
}