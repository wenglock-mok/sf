<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity()
 * @ORM\Table(name="languages", indexes={
 *     @ORM\Index(name="languages_alpha2code_idx", columns={"alpha2code"}),
 *     @ORM\Index(name="languages_alpha3code_idx", columns={"alpha3code"})
 * })
 *
 * Class Language
 * @package AppBundle\Entity
 */
class Language
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="text")
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="native_name", type="text", nullable=true)
     *
     * @var string
     */
    protected $nativeName;

    /**
     * @ORM\Column(name="alpha2code", type="string", length=2, nullable=true)
     *
     * @var string
     */
    protected $alpha2Code;

    /**
     * @ORM\Column(name="alpha3code", type="string", length=3, nullable=true)
     *
     * @var string
     */
    protected $alpha3Code;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Country", fetch="EXTRA_LAZY", inversedBy="languages")
     * @ORM\JoinTable(
     *     name="languages_countries",
     *     joinColumns={@ORM\JoinColumn(name="language", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="country", referencedColumnName="id")}
     * )
     *
     * @var Collection
     */
    protected $countries;

    /**
     * Language constructor.
     */
    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Language
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Language
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getNativeName()
    {
        return $this->nativeName;
    }

    /**
     * @param string $nativeName
     * @return Language
     */
    public function setNativeName($nativeName)
    {
        $this->nativeName = $nativeName;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlpha2Code()
    {
        return $this->alpha2Code;
    }

    /**
     * @param string $alpha2Code
     * @return Language
     */
    public function setAlpha2Code($alpha2Code)
    {
        $this->alpha2Code = $alpha2Code;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlpha3Code()
    {
        return $this->alpha3Code;
    }

    /**
     * @param string $alpha3Code
     * @return Language
     */
    public function setAlpha3Code($alpha3Code)
    {
        $this->alpha3Code = $alpha3Code;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param Collection $countries
     * @return Language
     */
    public function setCountries($countries)
    {
        $this->countries = $countries;
        return $this;
    }
}