<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="countries", indexes={
 *     @ORM\Index(name="countries_alpha2code_idx", columns={"alpha2code"}),
 *     @ORM\Index(name="countries_alpha3code_idx", columns={"alpha3code"}),
 *     @ORM\Index(name="countries_region_idx", columns={"region"}),
 *     @ORM\Index(name="countries_sub_region_idx", columns={"sub_region"})
 * })
 *
 * Class Country
 * @package AppBundle\Entity
 */
class Country
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="text")
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="tld", type="string", length=50, nullable=true)
     *
     * @var string
     */
    protected $topLevelDomain;

    /**
     * @ORM\Column(name="alpha2code", type="string", length=2, nullable=true)
     *
     * @var string
     */
    protected $alpha2Code;

    /**
     * @ORM\Column(name="alpha3code", type="string", length=3, nullable=true)
     *
     * @var string
     */
    protected $alpha3Code;

    /**
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\City", fetch="EXTRA_LAZY")
     * @ORM\JoinColumn(name="capital", referencedColumnName="id", nullable=true)
     *
     * @var City
     */
    protected $capital;

    /**
     * @ORM\Column(name="region", type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $region;

    /**
     * @ORM\Column(name="sub_region", type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $subRegion;

    /**
     * @ORM\Column(name="flag_url", type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $flagUrl;

    /**
     * @ORM\Column(name="calling_codes", type="text", nullable=true)
     *
     * @var string
     */
    protected $callingCodes;

    /**
     * @ORM\Column(name="eff_date", type="datetime")
     *
     * @var \DateTime
     */
    protected $effDate;

    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $endDate;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\City", fetch="EXTRA_LAZY", mappedBy="country")
     *
     * @var Collection
     */
    protected $cities;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Currency", fetch="EXTRA_LAZY", mappedBy="countries")
     *
     * @var Collection
     */
    protected $currencies;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Language", fetch="EXTRA_LAZY", mappedBy="countries")
     *
     * @var Collection
     */
    protected $languages;

    /**
     * Country constructor
     */
    public function __construct()
    {
        $this->endDate = null;
        $this->cities = new ArrayCollection();
        $this->currencies = new ArrayCollection();
        $this->languages = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Country
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getTopLevelDomain()
    {
        return $this->topLevelDomain;
    }

    /**
     * @param string $topLevelDomain
     * @return Country
     */
    public function setTopLevelDomain($topLevelDomain)
    {
        $this->topLevelDomain = $topLevelDomain;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlpha2Code()
    {
        return $this->alpha2Code;
    }

    /**
     * @param string $alpha2Code
     * @return Country
     */
    public function setAlpha2Code($alpha2Code)
    {
        $this->alpha2Code = $alpha2Code;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlpha3Code()
    {
        return $this->alpha3Code;
    }

    /**
     * @param string $alpha3Code
     * @return Country
     */
    public function setAlpha3Code($alpha3Code)
    {
        $this->alpha3Code = $alpha3Code;
        return $this;
    }

    /**
     * @return City
     */
    public function getCapital()
    {
        return $this->capital;
    }

    /**
     * @param City $capital
     * @return Country
     */
    public function setCapital(City $capital)
    {
        $this->capital = $capital;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion()
    {
        return $this->region;
    }

    /**
     * @param string $region
     * @return Country
     */
    public function setRegion($region)
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string
     */
    public function getSubRegion()
    {
        return $this->subRegion;
    }

    /**
     * @param string $subRegion
     * @return Country
     */
    public function setSubRegion($subRegion)
    {
        $this->subRegion = $subRegion;
        return $this;
    }

    /**
     * @return string
     */
    public function getFlagUrl()
    {
        return $this->flagUrl;
    }

    /**
     * @param string $flagUrl
     * @return Country
     */
    public function setFlagUrl($flagUrl)
    {
        $this->flagUrl = $flagUrl;
        return $this;
    }

    /**
     * @return array
     */
    public function getCallingCodes()
    {
        if (!empty($this->callingCodes)) {
            return json_decode($this->callingCodes, true);
        } else {
            return array();
        }
    }

    /**
     * @param array $callingCodes
     * @return Country
     */
    public function setCallingCodes(array $callingCodes = null)
    {
        if (!empty($callingCodes)) {
            $this->callingCodes = json_encode($callingCodes);
        } else {
            $this->callingCodes = null;
        }
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffDate()
    {
        return $this->effDate;
    }

    /**
     * @param \DateTime $effDate
     * @return Country
     */
    public function setEffDate(\DateTime $effDate)
    {
        $this->effDate = $effDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     * @return Country
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCities()
    {
        return $this->cities;
    }

    /**
     * @return Collection
     */
    public function getCurrencies()
    {
        return $this->currencies;
    }

    /**
     * @return Collection
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @return bool
     */
    public function isActive()
    {
        return empty($this->endDate);
    }
}