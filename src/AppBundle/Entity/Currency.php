<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="currencies", indexes={
 *     @ORM\Index(name="currencies_code_idx", columns={"code"})
 * })
 *
 * Class Currency
 * @package AppBundle\Entity
 */
class Currency
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @var int
     */
    protected $id;

    /**
     * @ORM\Column(name="name", type="text")
     *
     * @var string
     */
    protected $name;

    /**
     * @ORM\Column(name="code", type="string", length=50, nullable=true)
     *
     * @var string
     */
    protected $code;

    /**
     * @ORM\Column(name="symbol", type="string", length=255, nullable=true)
     *
     * @var string
     */
    protected $symbol;

    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Country", fetch="EXTRA_LAZY", inversedBy="currencies")
     * @ORM\JoinTable(
     *     name="currencies_countries",
     *     joinColumns={@ORM\JoinColumn(name="currency", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="country", referencedColumnName="id")}
     * )
     *
     * @var Collection
     */
    protected $countries;

    /**
     * @ORM\Column(name="eff_date", type="datetime")
     *
     * @var \DateTime
     */
    protected $effDate;

    /**
     * @ORM\Column(name="end_date", type="datetime", nullable=true)
     *
     * @var \DateTime
     */
    protected $endDate;

    /**
     * Currency constructor.
     */
    public function __construct()
    {
        $this->countries = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Currency
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Currency
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     * @return Currency
     */
    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }

    /**
     * @return string
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * @param string $symbol
     * @return Currency
     */
    public function setSymbol($symbol)
    {
        $this->symbol = $symbol;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCountries()
    {
        return $this->countries;
    }

    /**
     * @param Collection $countries
     * @return Currency
     */
    public function setCountries(Collection $countries)
    {
        $this->countries = $countries;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEffDate()
    {
        return $this->effDate;
    }

    /**
     * @param \DateTime $effDate
     * @return Currency
     */
    public function setEffDate(\DateTime $effDate)
    {
        $this->effDate = $effDate;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }

    /**
     * @param \DateTime $endDate
     * @return Currency
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;
        return $this;
    }
}