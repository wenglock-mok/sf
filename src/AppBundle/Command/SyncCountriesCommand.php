<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use AppBundle\Service\CountriesSyncService;

/**
 * Command to sync all countries data
 *
 * Class SyncCountriesCommand
 * @package AppBundle\Command
 */
class SyncCountriesCommand extends Command
{
    protected static $defaultName = 'sf:sync-countries';

    /**
     * @var CountriesSyncService
     */
    private $service;

    /**
     * SyncCountriesCommand constructor.
     * @param CountriesSyncService $service
     * @param string $name
     */
    public function __construct(CountriesSyncService $service, $name = null)
    {
        parent::__construct($name);
        $this->service = $service;
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Console\Command\Command::configure()
     */
    protected function configure()
    {
        // sets up the name of the command when run from the command line.
        $this
            ->setDescription('Syncs all countries');
    }

    /**
     * (non-PHPdoc)
     * @see \Symfony\Component\Console\Command\Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->service->syncAll();
    }
}