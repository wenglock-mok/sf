/**
 * This file is part of the AppBundle.
 *
 * The base angular service class from which all other service classes should inherit from.
 */

// we need to put this into a try catch block as we cannot be certain that sfApp has been loaded.
try {
    var app = angular.module('sfApp');

    app.factory(
        'BaseService',
        ['$http', '$q', function ($http, $q) {
            /**
             * An array to store all created deferred promises so we can cancel them if need be.
             */
            var deferreds = new Array();

            /**
             * Removes the given deferred promise from the array.
             * @var promise
             */
            function removeDeferred(deferred) {
                let i = deferreds.indexOf(deferred);
                if (i >= 0) {
                    deferreds.splice(i, 1);
                }
            }

            /**
             * Cancels all pending promises.
             */
            function cancelAll() {
                if (deferreds.length) {
                    angular.forEach(deferreds, function(deferred) {
                        deferred.reject('Base service cancelled request');
                    });
                }
            }

            /**
             * Performs a http GET request and returns a corresponding promise.
             * @var url The URL to perform the GET request to.
             * @return {Promise}
             */
            function doGet(url) {
                // perform the $http service call asynchronously and return a promise.
                let deferred = $q.defer();

                // push this into the list of deferred promises.
                deferreds.push(deferred);

                // run the http call to the remote service.
                $http({
                    method: 'GET',
                    url: url,
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest'
                    },
                    timeout: deferred.promise
                }).then(
                    function success(response) {
                        removeDeferred(deferred);
                        deferred.resolve(response.data);
                    },
                    function error(response) {
                        removeDeferred(deferred);
                        deferred.reject(response.data.error);
                    }
                );

                return deferred.promise;
            }

            /**
             * Performs a http POST request and returns a corresponding promise.
             * @var url The URL to perform the POST request to.
             * @var params A js object that contains the parameters to be sent to the POST request
             * @return {Promise}
             */
            function doPost(url, params) {
                // perform the $http service call asynchronously and return a promise.
                let deferred = $q.defer();

                // push this into the list of deferred promises.
                deferreds.push(deferred);

                // run the http call to the remote service to retrieve the rule translation.
                return $http({
                    method: 'POST',
                    url: url,
                    data: params,
                    headers: {
                        'X-Requested-With': 'XMLHttpRequest'
                    },
                    timeout: deferred.promise
                }).then(function success(response) {
                    return response.data;
                }).finally(function() {
                    removeDeferred(deferred);
                });

                return deferred.promise;
            }

            // return the service.
            return {
                cancelAll: cancelAll,
                doGet: doGet,
                doPost: doPost
            };
        }
        ]
    );
} catch (err) {
    console.log('Angular module sfApp has not been loaded yet.');
}
