/*
 * This file is part of the AppBundle.
 *
 * Declares the sfApp angularjs module
 */

if (!console) {
    window.console = {
        log: function() {}
    };
}

/**
 * Start angular module
 */
(function() {
    angular.module('sfApp', ['ui.bootstrap', 'ngMessages', 'ngAnimate', 'ngCookies', 'ngRoute']);
})();
